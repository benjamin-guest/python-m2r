Source: python-m2r
Section: python
Priority: optional
Maintainer: Jonas Smedegaard <dr@jones.dk>
Build-Depends:
 debhelper,
 dh-python,
 help2man <!nodoc>,
 python3,
 python3-docutils,
 python3-mistune,
 python3-mock <!nocheck>,
 python3-pygments <!nocheck>,
 python3-setuptools,
 python3-sphinx <!nodoc>,
Standards-Version: 4.4.1
Homepage: https://github.com/miyakogi/m2r
Vcs-Git: https://salsa.debian.org/debian/python-m2r.git
Vcs-Browser: https://salsa.debian.org/debian/python-m2r
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python

Package: python3-m2r
Architecture: all
Depends:
 python3-mistune,
 python3-docutils,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-m2r-doc,
Enhances:
 python3-sphinx
Description: Markdown and reStructuredText in a single file - Python3 library
 M2R converts a markdown file including reStructuredText (rst) markups
 to a valid rst format.
 .
 Use case is writing sphinx document in markdown,
 since it's widely used now and easy to write code blocks and lists.
 However, converters using pandoc or recommonmark
 do not support many rst markups and sphinx extensions.
 For example, rst's reference link like ``see `ref`_``
 (this is very convenient in long document
 in which same link appears multiple times)
 will be converted to a code block in HTML
 like `see <code>ref</code>_`,
 which is not expected.
 .
 This package provides m2r module
 for Python 3.

Package: m2r
Section: text
Architecture: all
Depends:
 python3-m2r (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-m2r-doc,
Enhances:
 python3-sphinx
Description: Markdown and reStructuredText in a single file - CLI tool
 M2R converts a markdown file including reStructuredText (rst) markups
 to a valid rst format.
 .
 Use case is writing sphinx document in markdown,
 since it's widely used now and easy to write code blocks and lists.
 However, converters using pandoc or recommonmark
 do not support many rst markups and sphinx extensions.
 For example, rst's reference link like ``see `ref`_``
 (this is very convenient in long document
 in which same link appears multiple times)
 will be converted to a code block in HTML
 like `see <code>ref</code>_`,
 which is not expected.
 .
 This package provides the command-line tool m2r.

Package: python-m2r-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 libjs-mathjax,
 ${sphinxdoc:Depends},
 ${misc:Depends},
Built-Using:
 ${sphinxdoc:Built-Using},
Description: Markdown and reStructuredText in a single file - documentation
 M2R converts a markdown file including reStructuredText (rst) markups
 to a valid rst format.
 .
 Use case is writing sphinx document in markdown,
 since it's widely used now and easy to write code blocks and lists.
 However, converters using pandoc or recommonmark
 do not support many rst markups and sphinx extensions.
 For example, rst's reference link like ``see `ref`_``
 (this is very convenient in long document
 in which same link appears multiple times)
 will be converted to a code block in HTML
 like `see <code>ref</code>_`,
 which is not expected.
 .
 This package provides documentation for the m2r module.
